<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\Server;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminIndex extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $TotalAttack = Attack::all()->count();
        $attackGoing = Attack::where('status', '=', 'On Going')->take(10)->get();
        $attackFinish = Attack::where('status', '=', 'Finished')->take(10)->get();
        $serverTotal = Server::where('name', '=','Server Total')->first();
        $serverAvailable = Server::where('name', '=','Server Available')->first();

    // $userResult = $this->usersCount($users);

        return view('admin.index', compact('TotalAttack','attackGoing', 'attackFinish', 'serverTotal', 'serverAvailable'));
    }

}
