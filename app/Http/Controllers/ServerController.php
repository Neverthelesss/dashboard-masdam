<?php

namespace App\Http\Controllers;

use App\Models\Server;
use DOMDocument;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class ServerController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:servers-create')->only(['create', 'store']);
        $this->middleware('can:servers-read')->only(['index', 'show']);
        $this->middleware('can:servers-update')->only(['edit', 'update']);
        $this->middleware('can:servers-delete')->only(['destroy', 'massDestroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $servers = Server::all();

        return view('server.index', compact('servers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('server.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'total' => ['required']
        ]);
        $server = Server::create([
            'name' => $request->name,
            'total' => $request->total,
        ]);
        return redirect()->route('admin.servers.index')->with('success', 'Setting created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $server = Server::findOrFail($id);

    //     return view('servers.show', compact('server'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $server = Server::findOrFail($id);

        return view('server.edit', compact('server'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required'],
            'total' => ['required']
        ]);

        $server = Server::findOrFail($id);

        $server->update([
            'name' => $request->name,
            'total' => $request->total,
        ]);

        return redirect()->route('admin.servers.index')->with('success', 'Setting updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $server = Server::findOrFail($id);

        $server->delete();

        if (request()->ajax()) {
            return response()->json(true);
        }

        return redirect()->route('admin.servers.index')->with('success', 'Setting deleted !');
    }

    public function massDestroy(Request $request)
    {
        $arr = explode(',', $request->ids);

        foreach ($arr as $id) {
            $server = Server::find($id);

            if ($server) {
                $servers_will_deleted[] = $server->id;
            }
        }

        Server::destroy($servers_will_deleted);

        if (request()->ajax()) {
            return response()->json(true);
        }

        return redirect()->route('admin.servers.index')->with('success', 'Settings deleted !');
    }
}
