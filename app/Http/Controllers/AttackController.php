<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use DOMDocument;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class AttackController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:attacks-create')->only(['create', 'store']);
        $this->middleware('can:attacks-read')->only(['index', 'show']);
        $this->middleware('can:attacks-update')->only(['edit', 'update']);
        $this->middleware('can:attacks-delete')->only(['destroy', 'massDestroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $attacks = Attack::all();

        return view('attack.index', compact('attacks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('attack.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'host' => ['required'],
            'status' => ['required']
        ]);
        $attack = Attack::create([
            'host' => $request->host,
            'status' => $request->status,
        ]);
        return redirect()->route('admin.attacks.index')->with('success', 'Setting created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $attack = Attack::findOrFail($id);

    //     return view('attacks.show', compact('attack'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attack = Attack::findOrFail($id);

        return view('attack.edit', compact('attack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'host' => ['required'],
            'status' => ['required']
        ]);

        $attack = Attack::findOrFail($id);

        $attack->update([
            'host' => $request->host,
            'status' => $request->status,
        ]);

        return redirect()->route('admin.attacks.index')->with('success', 'Setting updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attack = Attack::findOrFail($id);

        $attack->delete();

        if (request()->ajax()) {
            return response()->json(true);
        }

        return redirect()->route('admin.attacks.index')->with('success', 'Setting deleted !');
    }

    public function massDestroy(Request $request)
    {
        $arr = explode(',', $request->ids);

        foreach ($arr as $id) {
            $attack = Attack::find($id);

            if ($attack) {
                $attacks_will_deleted[] = $attack->id;
            }
        }

        Attack::destroy($attacks_will_deleted);

        if (request()->ajax()) {
            return response()->json(true);
        }

        return redirect()->route('admin.attacks.index')->with('success', 'Settings deleted !');
    }

}
