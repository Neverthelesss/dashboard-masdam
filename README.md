## About Base Laravel Project

This Base Laravel Project is developed.

Used Packages :
- AdminLTE
- Spatie Permission
- Datatables

Features :
- Account Control
- Roles & Permissions
- Users Control
- Blog
- Server-side Datatables
- Export options : csv, excel, pdf, and print
- CRUD with Bulk Delete
- API Authentication with Sanctum

To Run this Project : 
- 'composer install'
- 'php artisan migrate --seed'
- 'php artisan db:seed ServerSeeder'

going admin just add /admin in url 

credential 
super admin : super.admin@admin.com
password : password

admin : admin@admin.com
password : password

Thanks