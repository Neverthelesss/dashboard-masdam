@extends('layouts.veltrix')

@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-sm-12 col-lg-12">
                    <h6 class="page-title">Dashboard</h6>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Welcome to Attack Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-4 col-md-4 col-sm-12">
                <div class="card" style="background:#EC4561;">
                    <div class="card-body">
                        <div class="text-center text-white py-4">
                            <h3 class="mb-4 text-white font-size-18"><i class="fas fa-fire"></i> Total Attack</h3>
                            <h1>{{$TotalAttack}}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-4 col-sm-12">
                <div class="card" style="background:#EC4561;">
                    <div class="card-body">
                        <div class="text-center text-white py-4">
                            <h3 class="mb-4 text-white font-size-18"><i class="fas fa-server"></i> Server Available</h3>
                            <h1>{{$serverAvailable->total}}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-4 col-sm-12">
                <div class="card" style="background:#EC4561;">
                    <div class="card-body">
                        <div class="text-center text-white py-4">
                            <h3 class="mb-4 text-white font-size-18"><i class="fas fa-server"></i> Total Server </h3>
                            <h1>{{$serverTotal->total}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xl-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">On Going Attack</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-centered table-nowrap text-center mb-0">
                                <thead>
                                    <tr>
                                        <th width="25%" scope="col">No</th>
                                        <th scope="col">host</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($attackGoing as $going)
                                    <th scope="row"> {{$loop->index+1 }} </th>
                                        
                                    <td> {{$going->host}} </td>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Finished Attack</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-centered table-nowrap text-center mb-0">
                                <thead>
                                    <tr>
                                        <th width="25%" scope="col">No</th>
                                        <th scope="col">host</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($attackFinish as $finish)
                                    <th scope="row"> {{$loop->index+1 }} </th>
                                        
                                    <td> {{$finish->host}} </td>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->



    </div> <!-- container-fluid -->
</div>
@endsection

@section('script')
    <!-- ChartJS -->
    <script src="{{ asset('adminlte/plugins/chart.js/Chart.min.js') }}"></script>
@endsection
