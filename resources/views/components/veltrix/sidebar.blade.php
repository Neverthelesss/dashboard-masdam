 <!-- ========== Left Sidebar Start ========== -->
 <div class="vertical-menu">

    <div data-simplebar="dark" class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{route('admin.index')}}" class=" waves-effect">
                        <i class="fas fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @can('systems-control')
                <li class="menu-title">System Control</li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-cog"></i>
                        <span>Setting</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('admin.attacks.index')}}"><i class="fas fa-fire"></i> Attack</a></li>
                        <li><a href="{{route('admin.servers.index')}}"><i class="fas fa-server"></i> Server</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-hdd"></i>
                        <span>Permission Access</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @can('permissions-read')
                        <li>
                            <a href="{{ route('admin.permissions.index') }}"{{ request()->routeIs('admin.permissions.*') ? 'active' : '' }}">
                                <i class="fas fa-life-ring"></i>
                                
                                    Permissions
                                    {{-- <span class="right badge badge-danger">New</span> --}}
                                
                            </a>
                        </li>
                    @endcan
                    @can('roles-read')
                        <li>
                            <a href="{{ route('admin.roles.index') }}"{{ request()->routeIs('admin.roles.*') ? 'active' : '' }}">
                                <i class="far fa-life-ring"></i>
                        
                                    Roles
                                    {{-- <span class="right badge badge-danger">New</span> --}}
                                
                            </a>
                        </li>
                    @endcan
                    @can('users-read')
                        <li>
                            <a href="{{ route('admin.users.index') }}"{{ request()->routeIs('admin.users.*') ? 'active' : '' }}">
                                <i class="fas fa-users-cog"></i>
                                
                                    Users
                                    {{-- <span class="right badge badge-danger">New</span> --}}
                                
                            </a>
                        </li>
                    @endcan
                    </ul>
                </li>
                @endcan

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->