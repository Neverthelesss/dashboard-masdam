@extends('layouts.veltrix')

@section('head')
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp">
    <style>
        .ck-editor__editable_inline {
            min-height: 400px;
        }

    </style>
@endsection

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <h1 class="page-title m-0">Edit attack</h1>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.attacks.index') }}">attacks</a></li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
                </div>
            </div>
            {{-- content --}}
            <div class="row">
                <div class="card m-0">
                    <div class="card-body">
                        <form action="{{ route('admin.attacks.update', $attack) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="host" class="text-capitalize">host</label>
                                        <input type="text" class="form-control" id="host" name="host" placeholder="Some Title Here"
                                            value="{{ old('host') ?? $attack->host }}" required>
                                        @error('host')
                                            <div class="text-sm text-danger">{{ $message ?? 'Something error' }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="status" class="text-capitalize">status</label>
                                        <select name="status" class="form-control">
                                            <option value="On Going">On Going</option>
                                            <option value="Status">Status</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm my-3">Update</button>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    
@endsection

@section('script')
    <script src="{{ asset('ckeditor5/build/ckeditor.js') }}"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#body'), {

                licenseKey: '',



            })
            .then(editor => {
                window.editor = editor;




            })
            .catch(error => {
                console.error('Oops, something went wrong!');
                console.error(
                    'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:'
                );
                console.warn('Build id: 8ubb9kaqv8bd-mk6bg6wswnw1');
                console.error(error);
            });
    </script>
@endsection
