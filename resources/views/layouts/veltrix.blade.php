<!DOCTYPE html>
<html lang="en">

<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Dashboard | Veltrix - Admin & Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('veltrix/assets/images/favicon-1.ico')}}">

    <link href="{{asset('veltrix/assets/libs/chartist/chartist.min-1.css')}}" rel="stylesheet">

    <!-- Bootstrap Css -->
    <link href="{{asset('veltrix/assets/css/bootstrap.min-1.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Icons Css -->
    <link href="{{asset('veltrix/assets/css/icons.min-1.css')}}" rel="stylesheet" type="text/css">
    <!-- App Css-->
    <link href="{{asset('veltrix/assets/css/app.min-1.css')}}" id="app-style" rel="stylesheet" type="text/css">
    @yield('head')
</head>

<body data-sidebar="dark" data-bs-theme="dark">
    <div id="layout-wrapper">
        <x-veltrix.navbar />
        <x-veltrix.sidebar />

        {{-- Content Wrapper. Contains page content --}}
        <div class="main-content">
            
            @yield('content')
        </div>
        {{-- content-wrapper --}}

        {{-- <x-adminlte.control-sidebar />
        <x-adminlte.footer /> --}}
        <form action="{{ route('logout') }}" method="post" id="logoutForm">
            @csrf
        </form>
    </div>

    <!-- JAVASCRIPT -->
    <script src="{{asset('veltrix/assets/libs/jquery/jquery.min-1.js')}}"></script>
    <script src="{{asset('veltrix/assets/libs/bootstrap/js/bootstrap.bundle.min-1.js')}}"></script>
    <script src="{{asset('veltrix/assets/libs/metismenu/metisMenu.min-1.js')}}"></script>
    <script src="{{asset('veltrix/assets/libs/simplebar/simplebar.min-1.js')}}"></script>
    <script src="{{asset('veltrix/assets/libs/node-waves/waves.min-1.js')}}"></script>


    <!-- Peity chart-->
    <script src="{{asset('veltrix/assets/libs/peity/jquery.peity.min-1.js')}}"></script>

    <!-- Plugin Js-->
    <script src="{{asset('veltrix/assets/libs/chartist/chartist.min-1.js')}}"></script>
    <script src="{{asset('veltrix/assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min-1.js')}}"></script>

    <script src="{{asset('veltrix/assets/js/pages/dashboard.init-1.js')}}"></script>

    <script src="{{asset('veltrix/assets/js/app-1.js')}}"></script>
    @yield('script')
</body>

</html>
